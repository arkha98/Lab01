$( document ).ready(function() {
  var button_0 = $('button:contains("0")');
  var button_1 = $('button:contains("1")');
  var button_2 = $('button:contains("2")');
  var button_3 = $('button:contains("3")');
  var button_4 = $('button:contains("4")');
  var button_5 = $('button:contains("5")');
  var button_6 = $('button:contains("6")');
  var button_7 = $('button:contains("7")');
  var button_8 = $('button:contains("8")');
  var button_9 = $('button:contains("9")');

  var button_add = $('button:contains("+")');
  var button_sub = $('button:contains("-")');
  var button_mul = $('button:contains("*")');
  var button_div = $('button:contains("/")');
  var button_log = $('button:contains("log")');
  var button_sin = $('button:contains("sin")');
  var button_tan = $('button:contains("tan")');

  var button_dot = $('button:contains(".")');
  var button_clear = $('button:contains("AC")');
  var button_res = $('button:contains("=")');

  QUnit.test( "Addition Test", function( assert ) {
    button_1.click();
    button_0.click();
    button_add.click();
    button_3.click();
    button_res.click();
    assert.equal( $('#print').val(), 13, "10 + 3 = 13" );
    button_clear.click();
  });

  QUnit.test( "Substraction Test", function( assert ) {
    button_9.click();
    button_9.click();
    button_9.click();
    button_sub.click();
    button_9.click();
    button_8.click();
    button_6.click();
    button_res.click();
    assert.equal( $('#print').val(), 13, "999 - 986 = 13" );
    button_clear.click();
  });

  QUnit.test( "Multiply Test", function( assert ) {
    button_1.click();
    button_1.click();
    button_mul.click();
    button_3.click();
    button_res.click();
    assert.equal( $('#print').val(), 33, "11 * 3 = 33" );
    button_clear.click();
  });

  QUnit.test( "Division Test", function( assert ) {
    button_3.click();
    button_3.click();
    button_div.click();
    button_3.click();
    button_res.click();
    assert.equal( $('#print').val(), 11, "33 / 3 = 11" );
    button_clear.click();
  });

  QUnit.test( "Logarithm Test", function( assert ) {
    button_1.click();
    button_0.click();
    button_log.click();
    assert.equal( $('#print').val(), 1, "log(10) = 1");
    button_clear.click();
  });

  QUnit.test( "Sin Test", function( assert ) {
    button_1.click();
    button_dot.click();
    button_5.click();
    button_7.click();
    button_sin.click();
    assert.equal( $('#print').val(), 1, "sin(1.57) = 1");
  });

  QUnit.test( "Tan Test", function ( assert ) {
    button_0.click();
    button_tan.click();
    assert.equal( $('#print').val(), 0, "tan(0) = 0");
  })

});
