Running with gitlab-ci-multi-runner 9.5.0 (413da38)
  on docker-auto-scale (e11ae361)
Using Docker executor with image python:3.6 ...
Using docker image sha256:8c9dbad7d9c02f93a11b2923430be1f271ba13de27f3245103dbe6014767df18 for predefined container...
Pulling docker image python:3.6 ...
Using docker image python:3.6 ID=sha256:d3e1aad6d2e7229a93be6dd5fa3c5ec3d9845cc9f854dce808c12d89df7690af for build container...
Running on runner-e11ae361-project-4054917-concurrent-0 via runner-e11ae361-machine-1505199653-5214c1a5-digital-ocean-2gb...
Cloning repository...
Cloning into '/builds/arkha98/Lab01'...
Checking out f0f4b569 as master...
Skipping Git submodules setup
$ pip install -r requirements.txt
Collecting astroid==1.5.3 (from -r requirements.txt (line 1))
  Downloading astroid-1.5.3-py2.py3-none-any.whl (269kB)
Collecting colorama==0.3.9 (from -r requirements.txt (line 2))
  Downloading colorama-0.3.9-py2.py3-none-any.whl
Collecting coverage==4.4.1 (from -r requirements.txt (line 3))
  Downloading coverage-4.4.1-cp36-cp36m-manylinux1_x86_64.whl (196kB)
Collecting Django==1.11.4 (from -r requirements.txt (line 4))
  Downloading Django-1.11.4-py2.py3-none-any.whl (6.9MB)
Collecting gunicorn==19.7.1 (from -r requirements.txt (line 5))
  Downloading gunicorn-19.7.1-py2.py3-none-any.whl (111kB)
Collecting isort==4.2.15 (from -r requirements.txt (line 6))
  Downloading isort-4.2.15-py2.py3-none-any.whl (43kB)
Collecting lazy-object-proxy==1.3.1 (from -r requirements.txt (line 7))
  Downloading lazy_object_proxy-1.3.1-cp36-cp36m-manylinux1_x86_64.whl (55kB)
Collecting mccabe==0.6.1 (from -r requirements.txt (line 8))
  Downloading mccabe-0.6.1-py2.py3-none-any.whl
Collecting pylint==1.7.2 (from -r requirements.txt (line 9))
  Downloading pylint-1.7.2-py2.py3-none-any.whl (644kB)
Collecting pytz==2017.2 (from -r requirements.txt (line 10))
  Downloading pytz-2017.2-py2.py3-none-any.whl (484kB)
Collecting six==1.10.0 (from -r requirements.txt (line 11))
  Downloading six-1.10.0-py2.py3-none-any.whl
Collecting wrapt==1.10.11 (from -r requirements.txt (line 12))
  Downloading wrapt-1.10.11.tar.gz
Building wheels for collected packages: wrapt
  Running setup.py bdist_wheel for wrapt: started
  Running setup.py bdist_wheel for wrapt: finished with status 'done'
  Stored in directory: /root/.cache/pip/wheels/56/e1/0f/f7ccf1ed8ceaabccc2a93ce0481f73e589814cbbc439291345
Successfully built wrapt
Installing collected packages: six, lazy-object-proxy, wrapt, astroid, colorama, coverage, pytz, Django, gunicorn, isort, mccabe, pylint
Successfully installed Django-1.11.4 astroid-1.5.3 colorama-0.3.9 coverage-4.4.1 gunicorn-19.7.1 isort-4.2.15 lazy-object-proxy-1.3.1 mccabe-0.6.1 pylint-1.7.2 pytz-2017.2 six-1.10.0 wrapt-1.10.11
$ python manage.py makemigrations
No changes detected
$ python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying sessions.0001_initial... OK
$ coverage run --include='lab_*/*' manage.py test
..............
----------------------------------------------------------------------
Ran 14 tests in 0.027s

OK
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
Destroying test database for alias 'default'...
$ coverage report -m
Name                                 Stmts   Miss  Cover   Missing
------------------------------------------------------------------
lab_1/__init__.py                        0      0   100%
lab_1/admin.py                           1      0   100%
lab_1/migrations/__init__.py             0      0   100%
lab_1/models.py                          1      0   100%
lab_1/tests.py                          30      0   100%
lab_1/urls.py                            3      0   100%
lab_1/views.py                          10      0   100%
lab_2/__init__.py                        0      0   100%
lab_2/admin.py                           1      0   100%
lab_2/migrations/__init__.py             0      0   100%
lab_2/models.py                          1      0   100%
lab_2/tests.py                          22      0   100%
lab_2/urls.py                            3      0   100%
lab_2/views.py                           6      0   100%
lab_2_addon/__init__.py                  0      0   100%
lab_2_addon/migrations/__init__.py       0      0   100%
lab_2_addon/tests.py                    32      0   100%
lab_2_addon/urls.py                      3      0   100%
lab_2_addon/views.py                     7      0   100%
------------------------------------------------------------------
TOTAL                                  120      0   100%
Job succeeded
