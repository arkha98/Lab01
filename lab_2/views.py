from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Halo nama saya Arkha Sayoga Mayadi. Saya adalah mahasiswa Fakultas Ilmu Komputer, Universitas Indonesia jurusan Ilmu Komputer. Hobi saya adalah membuat robot. Ada beberapa robot yang pernah saya buat salah satunya adalah Robot Sumo. Robot ini rencananya akan saya ikut sertakan di salah satu lomba tingkat nasional.'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)