# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

def index(request):
    response = {}
    html = 'lab_8/lab_8.html'
    return render(request, html, response)

